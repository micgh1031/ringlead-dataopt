require.config({
    baseUrl: '/static',
    paths: {
        vue: 'vue',
        moment: 'js/moment'
    }
});

require([
    'vue',
    'moment',

    '/static/components/program_details.js',
    '/static/components/create_program_modal.js',
    '/static/components/program_list.js',
    '/static/components/notification_list.js',
    '/static/components/notification_bar.js',

], function(Vue, moment, ProgramDetailsComponent, CreateProgramModalComponent,
            ProgramListComponent, NotificationListComponent,
            NotificationBarComponent) {

    Vue.config.delimiters = ['{$', '$}'];
    Vue.config.unsafeDelimiters = ['{$$', '$$}'];
    Vue.config.debug = true;

    Vue.filter('calendarDate', function (value) {
        return moment(value).calendar();
    });

    Vue.filter('truncate', function (value, chars) {
        if (value.length > chars) {
            value = value.substr(0, chars-1) + '…';
        }
        return value;
    });

    $(function(){
        new Vue({
            el: 'body',

            props: ['consumeEvents'],

            data: function() {
                return {
                    // events marked as consumed
                    consumedEvents: [],

                    // fetch call options
                    fetchDelay: 5000,
                    fetchTimeout: null,
                    fetchActive: false
                }
            },

            components: {
                'program-list': ProgramListComponent,

                'notification-list': NotificationListComponent,
                'notification-bar': NotificationBarComponent,

                'create-program-modal': CreateProgramModalComponent,
                'program-details': ProgramDetailsComponent
            },

            ready: function() {
                this.fetchData();

                // swap loading message
                $('#wrapper').show();
            },

            events: {
                fetchData: function() {
                    if (this.fetchActive)
                        return;

                    var self = this, delay = 5000;

                    // block fetch
                    this.fetchActive = true;

                    // recursive
                    clearTimeout(this.fetchTimeout);

                    // consumedEvents
                    var consumedEvents = [];
                    while (this.consumedEvents.length > 0)
                        consumedEvents.push(this.consumedEvents.pop());

                    $.ajax({
                        url: '/events.json',
                        data: {
                            'consume_events': this.consumeEvents,
                            'consumed_events': consumedEvents.join(',')
                        },
                        dataType: 'json',
                        success: function(data) {
                            // notify components
                            self.$broadcast('eventReceived', data);
                        },
                        complete: function() {
                            self.fetchActive = false;
                            self.fetchTimeout = setTimeout(function(){
                                self.$emit('fetchData');
                            }, self.fetchDelay);
                        }
                    });
                },

                markConsumed: function(eventIds) {
                    for (var i = 0; i < eventIds.length; ++i)
                        this.consumedEvents.push(eventIds[i]);

                    this.$emit('fetchData');
                }
            },

            methods: {
                fetchData: function() {
                    this.$emit('fetchData');
                },
            }
        });
    });

});
