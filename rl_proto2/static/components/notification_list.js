define([
    '/static/components/notification.js'

], function(Notification){
    return Notification.extend({

        replace: false,

        data: function() {
            return {
                eventRefs: {} // store reference between events and notifications
            }
        },

        events: {
            eventReceived: function(event) {
                if ( ! event.notification)
                    return;

                var self = this;

                event.notification.forEach(function(e){
                    self.eventRefs[e.event_data.id] = e.event_id;
                });
            }
        },

        methods: {
            showNotificationItem: function(subject, text, id) {
                this.currentNotification = {
                    'subject': subject,
                    'text': text,
                    'event': {
                        'id': this.eventRefs[id] || 0
                    }
                };

                this.showNotification();
            },

            archiveNotification: function(id) {
                $.ajax({
                    url: '/notifications/archive_notification/',
                    data: {'pk': id},
                    success: function (data, text_status, jqXHR) {
                        if (data['result'] == 'success') {
                            var bloop = true;
                            var cid = data['notification'].id;
                            $("#notification_table").find("tr").each(function(){
                                if ( bloop ) {
                                    var notification_id = $(this).attr('notificationid');
                                    if (notification_id == cid) {
                                        $(this).remove();
                                        bloop = false;
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }

    });
});
