define([
    'vue',

    '/static/components/program_type_selection.js',
    '/static/components/list_import_dropzone.js',

], function(Vue, ProgramTypeSelectionComponent, ListImportDropzoneComponent){
    return Vue.extend({

        data: function() {
            return {
                title: 'Create a New Data Task',
                currentView: 'program-type-selection',
                currentViewProps: {},
            }
        },

        components: {
            'program-type-selection': ProgramTypeSelectionComponent,
            'list-import-dropzone': ListImportDropzoneComponent
        },

        events: {
            'view-change': function (viewName, viewProps) {
                this.currentViewProps = viewProps || {};
                this.$set('currentView', viewName);
            },

            eventReceived: function(event) {
                // re-broadcast to child components
                this.$broadcast('eventReceived', event);
            }
        },

        ready: function () {
            var self = this;

            $('#newProgramModal').on('hidden.bs.modal', function (e) {
                self.$set('currentView', 'program-type-selection');
            });
        }

    });
});
