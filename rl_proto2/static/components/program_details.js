define([
    'vue',
    'moment',

], function(Vue, moment) {
    return Vue.extend({
        replace: false,

        data: function() {
            return {
                program: {},
                selectedReportType: 'snapshot',
                fetchInterval: null,
                editNameMode: false,
                gage: {},
                scoreRanges: [
                    {low: 0, high: 25, label: 'Extremely Poor'},
                    {low: 26, high: 50, label: 'Very Poor'},
                    {low: 51, high: 65, label: 'Poor'},
                    {low: 66, high: 75, label: 'Average'},
                    {low: 76, high: 85, label: 'Above Average'},
                    {low: 86, high: 90, label: 'Great'},
                    {low: 91, high: 100, label: 'Excellent'}
                ]
            }
        },

        computed: {
            lastRun: function() {
                if (this.program.last_run) {
                    return moment(this.program.last_run).fromNow();
                }
            }
        },

        methods: {
            disableUi: function() {
                $('#program-details .btn').addClass('disabled');
            },

            enableUi: function() {
                $('#program-details .btn').removeClass('disabled');
            },

            getProgramPk: function() {
                if (!$.isEmptyObject(this.program)) {
                    return this.program.id;
                }

                var pkHolder = $('#program-pk-holder');

                if (pkHolder.length) {
                    return pkHolder.data('pk');
                }
            },

            fetch: function() {
                var self = this;

                $.ajax({
                    url: '/api/program/' + self.getProgramPk() + '/',
                    method: 'GET',
                    success: function(data, text_status, jqXHR) {
                        self.$set('program', data);
                        self.$nextTick(self._onProgramSet);
                    }
                });
            },

            _onProgramSet: function() {
                if (this.gage.originalValue != this.program.current_quality_score.score) {
                    this._setupGage();
                }
                this._setupTagit();

                if (this.selectedReportType == 'scoreOverTime') {
                    this._setupQotChart();
                }

                if (this.program.status == 'RUN') {
                    if (!this.fetchInterval) {
                        this.gage = {};
                        this.fetchInterval = setInterval(this.fetch, 3000);
                    }
                }

                if (this.fetchInterval && this.program.progress == 100 &&
                    this.program.current_activity_description == null) {
                    clearInterval(this.fetchInterval);
                    this.fetchInterval = null;
                }
            },

            _setupGage: function() {
                var gageEl = $('#gage');

                if (gageEl.length) {
                    gageEl.html('');
                    this.gage = new JustGage({
                        id: 'gage',
                        value: this.program.current_quality_score.score || 0,
                        min: 0,
                        max: 100,
                        levelColors: ["#FF3300", "#FF9900", "#FFFF00", "#33CC33"],
                        label: this._getGageLabel()
                    });
                }
            },

            _getGageLabel: function() {
                var self = this;

                if ($.isEmptyObject(self.program)) {
                    return;
                }

                var label;
                $.each(this.scoreRanges, function (index, range) {
                    if (self.program.current_quality_score.score >= range.low
                        && self.program.current_quality_score.score <= range.high) {
                        label = range.label;
                        return false;
                    }
                });
                return label;
            },

            _onReportChange: function() {
                if (this.selectedReportType == 'scoreOverTime') {
                    this.$nextTick(this._setupQotChart);
                } else if (this.selectedReportType == 'snapshot') {
                    this.$nextTick(this._setupGage);
                }
            },

            _setupQotChart: function() {
                if (!this.program.quality_scores.length || !$('#canvas').length) {
                    return;
                }

                var self = this, labels = [], data = [];

                $.each(self.program.quality_scores, function (index, value) {
                    labels.push(moment(value.created).calendar());
                    data.push(value.score);
                });

                $('#canvas').replaceWith('<canvas id="canvas" height="600" width="450"></canvas>');

                var barChartData = {
                    labels: labels,
                    datasets: [
                        {
                            label: "Quality score over time",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: data
                        }
                    ]
                };

                var ctx = document.getElementById("canvas").getContext("2d");

                window.qotBar = new Chart(ctx).Bar(barChartData, {
                    //responsive: true,
                });
            },

            _setupTagit: function() {
                $('#program-tags').tagit({
                    allowSpaces: true,
                    placeholderText: 'insert, your, tags',
                    afterTagRemoved: this._updateTags,
                    afterTagAdded: this._updateTags
                });
            },

            _updateTags: function(event, ui) {
                if (ui.duringInitialization) {
                    return;
                }

                var self = this;

                $.ajax({
                    url: '/program_set_tags/',
                    method: 'GET',
                    data: {
                        pk: this.getProgramPk(),
                        tags: $('#program-tags').val()
                    },
                    success: function(data, text_status, jqXHR) {
                        self.fetch();
                    }
                });
            },

            _processProgramAction: function(actionUrl) {
                var self = this;

                self.disableUi();

                $.ajax({
                    url: actionUrl,
                    method: 'GET',
                    success: function(data, text_status, jqXHR) {
                        self.$set('program', data);
                        self.$nextTick(self._onProgramSet);
                        self.enableUi();
                    }
                });
            },
            
            editSettings: function (autorun) {
                window.location = '/select_actions/' + this.getProgramPk() + (!!autorun ? '#run' : '');
            },

            setDataOrigin: function(event) {
            $.ajax({
                url: '/program/' + this.getProgramPk() + '/data_origin/',
                method: 'GET',
                data: {
                'data_origin': event.target.value,
                }
            });
            },

            setSchedule: function(event) {
            var data = {};
            $.each($(event.target).closest('form').serializeArray(), function () {
                data[this.name] = this.value;
            });
            $.ajax({
                url: '/program/' + this.getProgramPk() + '/schedule/',
                method: 'GET',
                data: data,
            });
            },
            
            renameProgram: function() {
                var self = this;

                $.ajax({
                    url: '/program/' + this.getProgramPk() + '/rename/',
                    method: 'GET',
                    data: {
                        name: $('#program-name-input').val()
                    },
                    success: function(data, text_status, jqXHR) {
                        self.$set('editNameMode', false);
                        self.$set('program', data);
                        self.$nextTick(self._onProgramSet);
                    }
                });
            }
        },

        ready: function () {
            var self = this;

            self.fetch();

            $('#program-name-contenteditable').on('blur', function(e) {
                self.renameProgram();
            });

            if (window.location.hash == '#run') {
                window.location.hash = ' ';
                this._processProgramAction('/program/' + this.getProgramPk() + '/optimize/');
            }
            
            $('#id_schedule_hour').timepicker({
              step: 15,
              forceRoundTime: true,
              scrollDefault: 'now',
            });
            $('#id_schedule_hour').on('change', function (event) {
              this.latest_valid_schedule_hour = event.target.value;
            });
            //$('#id_schedule_hour').timepicker('setTime', new Date()).change();
            $('#id_schedule_hour').on('timeFormatError', function (event) {
              $(this).val(this.latest_valid_schedule_hour);
            });
            $('.schedule-timezone').selectpicker({
              liveSearch: true,
              style: 'selectpicker selectpicker-flat schedule-timezone',
            });
            $('.schedule-toggle').bootstrapToggle();
        }
    });
});
