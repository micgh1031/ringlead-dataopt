# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0019_auto_20160222_2033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='error',
            field=models.TextField(null=True, blank=True),
        ),
    ]
