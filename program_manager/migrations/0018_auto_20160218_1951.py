# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0017_program_schedule_timezone'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='schedule_local_day',
            field=models.CharField(max_length=64, null=True, choices=[(b'1', b'Monday'), (b'2', b'Tuesday'), (b'3', b'Wednesday'), (b'4', b'Thursday'), (b'5', b'Friday'), (b'6', b'Saturday'), (b'0', b'Sunday')]),
        ),
        migrations.AddField(
            model_name='program',
            name='schedule_local_hour',
            field=models.TimeField(null=True),
        ),
    ]
