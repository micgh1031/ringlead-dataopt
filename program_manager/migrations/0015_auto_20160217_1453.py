# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('program_manager', '0014_program_schedule_task'),
    ]

    operations = [
        migrations.AlterField(
            model_name='program',
            name='schedule_task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='djcelery.PeriodicTask', null=True),
        ),
    ]
