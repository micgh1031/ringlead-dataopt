# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('djcelery', '0001_initial'),
        ('program_manager', '0013_auto_20160216_2250'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='schedule_task',
            field=models.ForeignKey(blank=True, to='djcelery.PeriodicTask', null=True),
        ),
    ]
